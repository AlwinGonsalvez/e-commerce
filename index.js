const mongoose = require('mongoose');
const express = require('express')
const app = express()
const userRouter = require('./routers/user_route')
const categoryRouter = require('./routers/category_route')
const productRouter = require('./routers/product_route')
const port = 3000
require('dotenv').config()

app.use(express.json())

app.use("/api/users", userRouter)
app.use("/api/categories", categoryRouter)
app.use("/api/products", productRouter)
main().then(()=>console.log("db connected")).catch(err => console.log(err));


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
async function main() {
    await mongoose.connect(process.env.MONGO_DB);
  
    // use `await mongoose.connect('mongodb://user:password@127.0.0.1:27017/test');` if your database has auth enabled
  }