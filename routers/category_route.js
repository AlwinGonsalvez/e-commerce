const express = require('express')
const {saveCategory,allCategories,getCategoryById,categoryDelete,categoryUpdate} = require("../controllers/category_controller")
const router = express.Router();

router.post('/savecategory' , saveCategory)

router.get('/allcategories', allCategories)

router.get('/getcategorybyid/:id', getCategoryById);

router.delete('/categorydelete/:id', categoryDelete)

router.patch('/categoryupdate/:id', categoryUpdate)


module.exports = router;