const express = require('express')
const {signUp, userList, userDelete, userUpdate} = require ("../controllers/user_controller.js")
const router = express.Router();
  
router.post('/signup' , signUp)

router.get('/userlist', userList)

router.delete('/userdelete/:id', userDelete)

router.patch('/userupdate/:id', userUpdate)


module.exports = router;